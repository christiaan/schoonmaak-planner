# Running

Running this code requires [docker][] make sure it is installed and working properly

```bash
docker build -t schoonmaak-planner .
docker run -ti schoonmaak-planner
```

[docker]: https://docs.docker.com/install/
