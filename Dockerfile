FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_NO_INTERACTION=1


# Install php
RUN apt-get update -yqq && \
apt-get install git wget curl software-properties-common language-pack-en-base zip \
  php7.2-cli php7.2-xml php7.2-zip php7.2-json php7.2-mbstring php-luasandbox -yqq

ADD . /app

WORKDIR /app

# Install composer
RUN wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig && \
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
php composer-setup.php && \
php -r "unlink('composer-setup.php'); unlink('installer.sig');"

RUN php composer.phar install

CMD php bin/generatePlanning.php
