<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner;

class Werk
{
    private $beschrijving;
    private $tijdInMinuten;

    public function __construct(string $beschrijving, int $tijdInMinuten)
    {
        $this->beschrijving = $beschrijving;
        $this->tijdInMinuten = $tijdInMinuten;
    }

    public function beschrijving(): string
    {
        return $this->beschrijving;
    }

    public function tijdInMinuten(): int
    {
        return $this->tijdInMinuten;
    }
}
