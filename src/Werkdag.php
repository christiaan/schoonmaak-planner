<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner;

use DateTimeInterface;

class Werkdag
{
    private $datum;
    private $werkzaamheden;

    /**
     * @param DateTimeInterface $datum
     * @param Werk[]            $werkzaamheden
     */
    public function __construct(\DateTimeInterface $datum, array $werkzaamheden)
    {
        $this->datum = $datum;
        $this->werkzaamheden = $werkzaamheden;
    }

    public function datum(): DateTimeInterface
    {
        return $this->datum;
    }

    /**
     * @return Werk[]
     */
    public function werkzaamheden(): array
    {
        return $this->werkzaamheden;
    }
}
