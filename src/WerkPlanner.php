<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner;

use DateTimeInterface;

interface WerkPlanner
{
    /**
     * @param DateTimeInterface $dag
     *
     * @return Werk[]
     */
    public function planWerk(DateTimeInterface $dag): array;
}
