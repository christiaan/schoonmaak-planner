<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner;

use DatePeriod;

class Planning
{
    private $periode;
    private $werkdagen;

    /**
     * Planning constructor.
     *
     * @param DatePeriod $periode
     * @param Werkdag[]  $werkdagen
     */
    public function __construct(DatePeriod $periode, array $werkdagen)
    {
        $this->periode = $periode;
        $this->werkdagen = $werkdagen;
    }

    /**
     * @return Werkdag[]
     */
    public function werkdagen(): array
    {
        return $this->werkdagen;
    }
}
