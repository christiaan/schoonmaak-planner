<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\Infrastructure;

use Christiaan\SchoonmaakPlanner\SchoonmaakPlanner;
use Christiaan\SchoonmaakPlanner\WerkPlanners\KoelkastWerkPlanner;
use Christiaan\SchoonmaakPlanner\WerkPlanners\RamenLappenWerkPlanner;
use Christiaan\SchoonmaakPlanner\WerkPlanners\StofzuigenWerkPlanner;
use DateInterval;
use DateTimeImmutable;

class CliApplication
{
    public function __invoke()
    {
        $luaCode = file_get_contents(__DIR__.'/../../werkplanners/stofzuigen.lua');
        if (!$luaCode) {
            throw new \RuntimeException('Could not read lua code from file');
        }

        $planner = new SchoonmaakPlanner(
            new ChainWerkPlanner(
//                new StofzuigenWerkPlanner(),
                new LuaWerkPlanner($luaCode),
                new KoelkastWerkPlanner(),
                new RamenLappenWerkPlanner()
            )
        );

        $start = new DateTimeImmutable('now');
        $einde = $start->add(new DateInterval('P6M'));
        $planning = $planner->maakPlanningVoor($start, $einde);

        $renderer = new CsvPlanningRenderer();
        $renderer->renderPlanning($planning, 'php://output');
        echo PHP_EOL;
    }
}
