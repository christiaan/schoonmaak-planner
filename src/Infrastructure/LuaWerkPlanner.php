<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\Infrastructure;

use Christiaan\SchoonmaakPlanner\Werk;
use Christiaan\SchoonmaakPlanner\WerkPlanner;
use DateTimeInterface;
use LuaSandbox;

class LuaWerkPlanner implements WerkPlanner
{
    private $luaCode;
    private $sandbox;

    /**
     * LuaWerkPlanner constructor.
     *
     * @param string $luaCode
     * @param int    $memoryLimit Memory limit in bytes, default is 50MiB
     * @param int    $cpuLimit    Limit in seconds, or 0 for no limit
     */
    public function __construct(string $luaCode, int $memoryLimit = 52428800, int $cpuLimit = 0)
    {
        $this->luaCode = $luaCode;
        $this->sandbox = new LuaSandbox();
        $this->sandbox->setMemoryLimit($memoryLimit);
        $this->sandbox->setCPULimit(0 === $cpuLimit ? false : $cpuLimit);
    }

    /**
     * @param DateTimeInterface $dag
     *
     * @return Werk[]
     */
    public function planWerk(DateTimeInterface $dag): array
    {
        $werkzaamheden = [];

        $this->sandbox->registerLibrary('dag', [
            'isWeekdag' => function ($weekdagen) use ($dag) {
                return [in_array((int) $dag->format('w'), $weekdagen, true)];
            },
            'planWerk' => function ($beschrijving, $minuten) use (&$werkzaamheden) {
                $werkzaamheden[] = new Werk($beschrijving, $minuten);
            },
        ]);

        $this->sandbox->loadString($this->luaCode)->call();

        return $werkzaamheden;
    }
}
