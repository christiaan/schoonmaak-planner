<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\Infrastructure;

use Christiaan\SchoonmaakPlanner\Planning;
use Christiaan\SchoonmaakPlanner\PlanningRenderer;
use Christiaan\SchoonmaakPlanner\Werk;
use Christiaan\SchoonmaakPlanner\Werkdag;

class CsvPlanningRenderer implements PlanningRenderer
{
    private $delimiter;
    private $enclosure;
    private $escape;

    public function __construct($delimiter = ',', $enclosure = '"', $escape = '\\')
    {
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
    }

    public function renderPlanning(Planning $planning, string $filename)
    {
        $output = new \SplFileObject($filename, 'wb');
        $output->setCsvControl($this->delimiter, $this->enclosure, $this->escape);
        $output->fputcsv(['datum', 'werkzaamheden', 'beschikbare_tijd']);

        foreach ($planning->werkdagen() as $werkdag) {
            $output->fputcsv([
                $werkdag->datum()->format('d-m'),
                $this->beschrijvingWerkzaamheden($werkdag),
                $this->totaleTijdWerkzaamheden($werkdag),
            ]);
        }
    }

    private function beschrijvingWerkzaamheden(Werkdag $werkdag): string
    {
        return implode(', ', array_map(
            static function (Werk $werkzaamheid) {
                return $werkzaamheid->beschrijving();
            },
            $werkdag->werkzaamheden()
        ));
    }

    private function totaleTijdWerkzaamheden(Werkdag $werkdag)
    {
        $minuten = array_sum(array_map(
            static function (Werk $werkzaamheid) {
                return $werkzaamheid->tijdInMinuten();
            },
            $werkdag->werkzaamheden()
        ));

        return floor($minuten / 60).':'.$minuten % 60;
    }
}
