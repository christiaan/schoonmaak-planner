<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\Infrastructure;

use Christiaan\SchoonmaakPlanner\WerkPlanner;

class ChainWerkPlanner implements WerkPlanner
{
    private $werkPlanners;

    public function __construct(WerkPlanner ...$werkPlanners)
    {
        $this->werkPlanners = $werkPlanners;
    }

    public function planWerk(\DateTimeInterface $dag): array
    {
        $werkzaamheden = [];
        foreach ($this->werkPlanners as $werkPlanner) {
            $werk = $werkPlanner->planWerk($dag);
            if ($werk) {
                array_push($werkzaamheden, ...$werk);
            }
        }

        return $werkzaamheden;
    }
}
