<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner;

class SchoonmaakPlanner
{
    private $werkPlanner;

    public function __construct(WerkPlanner $werkPlanner)
    {
        $this->werkPlanner = $werkPlanner;
    }

    public function maakPlanningVoor(\DateTimeImmutable $start, \DateTimeImmutable $einde): Planning
    {
        $periode = new \DatePeriod($start, new \DateInterval('P1D'), $einde);

        $werkdagen = [];
        foreach ($periode as $dag) {
            $werkzaamheden = $this->werkPlanner->planWerk($dag);
            if ($werkzaamheden) {
                $werkdagen[] = new Werkdag($dag, $werkzaamheden);
            }
        }

        return new Planning($periode, $werkdagen);
    }
}
