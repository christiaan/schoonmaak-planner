<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner;

interface PlanningRenderer
{
    public function renderPlanning(Planning $planning, string $filename);
}
