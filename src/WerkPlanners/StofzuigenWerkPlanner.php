<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\WerkPlanners;

use Christiaan\SchoonmaakPlanner\Werk;
use Christiaan\SchoonmaakPlanner\WerkPlanner;
use DateTimeInterface;

class StofzuigenWerkPlanner implements WerkPlanner
{
    private $weekdagen = [
        2, // dinsdag, zondag is 0
        4, // donderdag
    ];

    /**
     * @param DateTimeInterface $dag
     *
     * @return Werk[]
     */
    public function planWerk(DateTimeInterface $dag): array
    {
        $werkzaamheden = [];

        if (in_array((int) $dag->format('w'), $this->weekdagen, true)) {
            $werkzaamheden[] = new Werk('Stofzuigen', 21);
        }

        return $werkzaamheden;
    }
}
