<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\WerkPlanners;

use Christiaan\SchoonmaakPlanner\Werk;
use Christiaan\SchoonmaakPlanner\WerkPlanner;
use DateTimeInterface;

class KoelkastWerkPlanner implements WerkPlanner
{
    /**
     * @param DateTimeInterface $dag
     *
     * @return Werk[]
     */
    public function planWerk(DateTimeInterface $dag): array
    {
        $werkzaamheden = [];

        if ($this->isEersteDinsdagOfDonderdagVanDeMaand($dag)) {
            $werkzaamheden[] = new Werk('Koelkast schoonmaken', 50);
        }

        return $werkzaamheden;
    }

    private function isEersteDinsdagOfDonderdagVanDeMaand(DateTimeInterface $dag)
    {
        $eersteDinsdag = new \DateTimeImmutable('first tuesday of '.$dag->format('Y-m'));
        $eersteDonderdag = new \DateTimeImmutable('first thursday of '.$dag->format('Y-m'));

        if ($eersteDinsdag < $eersteDonderdag) {
            return $eersteDinsdag->format('Y-m-d') === $dag->format('Y-m-d');
        }

        return $eersteDonderdag->format('Y-m-d') === $dag->format('Y-m-d');
    }
}
