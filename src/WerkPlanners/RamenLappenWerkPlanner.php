<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\WerkPlanners;

use Christiaan\SchoonmaakPlanner\Werk;
use Christiaan\SchoonmaakPlanner\WerkPlanner;
use DateTimeImmutable;
use DateTimeInterface;

class RamenLappenWerkPlanner implements WerkPlanner
{
    private $werkdagen = [
        0, // Zondag
        6, // Zaterdag
    ];

    /**
     * @param DateTimeInterface $dag
     *
     * @return Werk[]
     */
    public function planWerk(DateTimeInterface $dag): array
    {
        $werkzaamheden = [];

        if ($this->isLaatsteWerkdagVanDeMaand($dag)) {
            $werkzaamheden[] = new Werk('Ramen lappen', 35);
        }

        return $werkzaamheden;
    }

    private function isLaatsteWerkdagVanDeMaand(DateTimeInterface $dag)
    {
        if ($this->isWeekendDag($dag)) {
            return false;
        }

        $laatsteWerkdag = $this->laatsteWerkdagVanMaand($dag);

        return $laatsteWerkdag->format('Y-m-d') === $dag->format('Y-m-d');
    }

    private function laatsteWerkdagVanMaand(DateTimeInterface $dag): DateTimeInterface
    {
        $dag = new DateTimeImmutable('last day of '.$dag->format('Y-m'));
        while ($this->isWeekendDag($dag)) {
            $dag = $dag->modify('-1 day');
        }

        return $dag;
    }

    /**
     * @param DateTimeInterface $dag
     *
     * @return bool
     */
    private function isWeekendDag(DateTimeInterface $dag): bool
    {
        return in_array((int) $dag->format('w'), $this->werkdagen, true);
    }
}
