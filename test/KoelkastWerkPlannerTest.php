<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\Test;

use Christiaan\SchoonmaakPlanner\WerkPlanners\KoelkastWerkPlanner;
use PHPUnit\Framework\TestCase;

class KoelkastWerkPlannerTest extends TestCase
{
    /** @var KoelkastWerkPlanner */
    private $obj;

    protected function setUp(): void
    {
        $this->obj = new KoelkastWerkPlanner();
    }

    /**
     * @dataProvider valideDataProvider
     */
    public function test_valide_datums($datum, $expectedWerk)
    {
        $werk = $this->obj->planWerk(new \DateTimeImmutable($datum));

        self::assertCount($expectedWerk, $werk);
    }

    public function valideDataProvider()
    {
        return [
            ['2019-01-01', 1],
            ['2019-02-05', 1],
            ['2019-03-05', 1],
            ['2019-04-02', 1],
            ['2019-05-02', 1],
            ['2019-06-04', 1],
            ['2019-07-02', 1],
            ['2019-08-01', 1],
            ['2019-09-03', 1],
            ['2019-10-01', 1],
            ['2019-11-05', 1],
            ['2019-12-03', 1],
        ];
    }
}
