<?php

declare(strict_types=1);

namespace Christiaan\SchoonmaakPlanner\Test;

use Christiaan\SchoonmaakPlanner\WerkPlanners\RamenLappenWerkPlanner;
use PHPUnit\Framework\TestCase;

class RamenLappenWerkPlannerTest extends TestCase
{
    /** @var RamenLappenWerkPlanner */
    private $obj;

    protected function setUp(): void
    {
        $this->obj = new RamenLappenWerkPlanner();
    }

    /**
     * @dataProvider valideDataProvider
     */
    public function test_valide_datums($datum, $expectedWerk)
    {
        $werk = $this->obj->planWerk(new \DateTimeImmutable($datum));

        self::assertCount($expectedWerk, $werk);
    }

    public function valideDataProvider()
    {
        return [
            ['2019-01-31', 1],
            ['2019-02-27', 0],
            ['2019-02-28', 1],
            ['2019-03-01', 0],
            ['2019-03-29', 1],
            ['2019-03-31', 0],
            ['2019-04-30', 1],
            ['2019-05-31', 1],
            ['2019-06-28', 1],
            ['2019-07-31', 1],
            ['2019-08-30', 1],
            ['2019-09-30', 1],
            ['2019-10-31', 1],
            ['2019-11-29', 1],
            ['2019-11-30', 0],
            ['2019-12-31', 1],
        ];
    }
}
